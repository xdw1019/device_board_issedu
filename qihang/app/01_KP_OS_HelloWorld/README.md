# 启航KP_IOT主控板体验程序HelloWorld

本案例实现的是通过KP_IOT主控板串口，在串口日志中输出 Welcome to use KP_IOT! 字符串。

在KP_IOT开发板系统代码中，HelloWorld程序位于app目录下面，具体路径如下：

```
device/board/isoftstone/qihang/app/01_KP_OS_HelloWorld/
```
## 编译调试

#### 修改 BUILD.gn 文件

修改 device/board/isoftstone/qihang/app/路径下 BUILD.gn 文件，指定 `helloworld.c` 参与编译。

```c
 "01_KP_OS_HelloWorld:helloworld_example",
```

#### 运行结果

示例代码编译烧录代码后，按下开发板的RESET按键，通过串口助手查看日志。
```c
[KP] Welcome to use KP_IOT!
```
