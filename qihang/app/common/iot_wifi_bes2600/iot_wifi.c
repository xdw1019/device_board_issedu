/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "lwip/netif.h"
#include "lwip/netifapi.h"
#include "lwip/ip4_addr.h"
#include "cmsis_os2.h"
#include "wifi_device.h"
#include "wifi_error_code.h"
#include "ohos_init.h"
#include "iot_wifi.h"
#include "iot_bes2600_debug.h"

#define DEF_TIMEOUT 15
#define ONE_SECOND 1
#define SELECT_WIFI_SECURITYTYPE WIFI_SEC_TYPE_PSK

static void WiFiInit(void);
static void WaitSacnResult(void);
static int WaitConnectResult(void);
static void OnWifiScanStateChangedHandler(int state, int size);
static void OnWifiConnectionChangedHandler(int state, WifiLinkedInfo *info);
static void OnHotspotStaJoinHandler(StationInfo *info);
static void OnHotspotStateChangedHandler(int state);
static void OnHotspotStaLeaveHandler(StationInfo *info);

static int g_staScanSuccess = 0;
static int g_ConnectSuccess = 0;
static int ssid_count = 0;
WifiEvent g_wifiEventHandler = {0};
WifiErrorCode error;
extern struct netif if_wifi;

#define SELECT_WLAN_PORT "wlan0"

int WifiConnect(const char *ssid, const char *psk)
{
    WifiScanInfo *info = NULL;
    unsigned int size = WIFI_SCAN_HOTSPOT_LIMIT;
    static struct netif *g_lwip_netif = NULL;

    osDelay(200);
    LOG_I("<--System Init-->\r\n");

    // 初始化WIFI
    WiFiInit();

    // 使能WIFI
    if (EnableWifi() != WIFI_SUCCESS) {
        LOG_E("EnableWifi failed, error = %d\r\n", error);
        return -1;
    }

    // 判断WIFI是否激活
    if (IsWifiActive() == 0)
    {
        LOG_E("Wifi station is not actived.\r\n");
        return -1;
    }

    // 分配空间，保存WiFi信息
    info = malloc(sizeof(WifiScanInfo) * WIFI_SCAN_HOTSPOT_LIMIT);
    if (info == NULL) {
        return -1;
    }
    // 轮询查找WiFi列表
    do {
        // 重置标志位
        ssid_count = 0;
        g_staScanSuccess = 0;

        // 开始扫描
        Scan();

        // 等待扫描结果
        WaitSacnResult();

        // 获取扫描列表
        error = GetScanInfoList(info, &size);

    } while (g_staScanSuccess != 1);
    
    for (uint8_t i = 0; i < ssid_count; i++) {
        LOG_I("no:%03d, ssid:%-30s, rssi:%5d\r\n", i+1, info[i].ssid, info[i].rssi/100);
    }

    // 连接指定的WiFi热点
    for (uint8_t i = 0; i < ssid_count; i++) {
        if (strcmp(ssid, info[i].ssid) == 0) {
            int result;
            LOG_I("Select:%3d wireless, Waiting...\r\n", i+1);
            // 拷贝要连接的热点信息
            WifiDeviceConfig select_ap_config = {0};
            strcpy(select_ap_config.ssid, info[i].ssid);
            strcpy(select_ap_config.preSharedKey, psk);
            select_ap_config.securityType = SELECT_WIFI_SECURITYTYPE;

            if (AddDeviceConfig(&select_ap_config, &result) == WIFI_SUCCESS) {
                if (ConnectTo(result) == WIFI_SUCCESS ) {
                    LOG_I("WiFi connect succeed!\r\n");
                    g_lwip_netif = netifapi_netif_find_by_name(SELECT_WLAN_PORT);
                    LOG_I("g_lwip_netif.full_name %s!\r\n", g_lwip_netif->full_name);
                    LOG_I("g_lwip_netif.hostname %s!\r\n", g_lwip_netif->hostname);
                    
                    uint32_t ip = ((ip4_addr_t *)&g_lwip_netif->ip_addr)->addr;
                    uint32_t gw = ((ip4_addr_t *)&g_lwip_netif->gw)->addr;
                    uint32_t mask = ((ip4_addr_t *)&g_lwip_netif->netmask)->addr;
                    LOG_I("net_intf_status_change_cb **ip = %s\n", inet_ntoa(ip));
                    LOG_I("net_intf_status_change_cb **netmask = %s\n", inet_ntoa(mask));
                    LOG_I("net_intf_status_change_cb **gw = %s\n", inet_ntoa(gw));
                    
                    LOG_I("WiFi connect succeed!\r\n");
                    break;
                }
            }
        }

        if (i == ssid_count - 1) {
            LOG_E("ERROR: No wifi as expected\r\n");
            while(1) osDelay(100);
        }
    }

    // 等待DHCP
    for(;;) {
        if(dhcp_is_bound(g_lwip_netif) == ERR_OK) {
                uint32_t ip = ((ip4_addr_t *)&g_lwip_netif->ip_addr)->addr;
                uint32_t gw = ((ip4_addr_t *)&g_lwip_netif->gw)->addr;
                uint32_t mask = ((ip4_addr_t *)&g_lwip_netif->netmask)->addr;
                LOG_I("net_intf_status_change_cb **ip = %s\n", inet_ntoa(ip));
                LOG_I("net_intf_status_change_cb **netmask = %s\n", inet_ntoa(mask));
                LOG_I("net_intf_status_change_cb **gw = %s\n", inet_ntoa(gw));
            LOG_I("<-- DHCP state:OK -->\r\n");
            // 打印获取到的IP信息
            break;
        }
        LOG_I("<-- DHCP state:Inprogress -->\r\n");
        osDelay(100);
    }

    osDelay(100);

    return 0;
}

/****************************************************
* 初始化WIFI
****************************************************/
static void WiFiInit(void)
{
    LOG_I("<--Wifi Init-->\r\n");
    g_wifiEventHandler.OnWifiScanStateChanged = OnWifiScanStateChangedHandler;
    //g_wifiEventHandler.OnWifiConnectionChanged = OnWifiConnectionChangedHandler;
    g_wifiEventHandler.OnHotspotStaJoin = OnHotspotStaJoinHandler;
    g_wifiEventHandler.OnHotspotStaLeave = OnHotspotStaLeaveHandler;
    g_wifiEventHandler.OnHotspotStateChanged = OnHotspotStateChangedHandler;
    error = RegisterWifiEvent(&g_wifiEventHandler);
    if (error != WIFI_SUCCESS) {
        LOG_E("register wifi event fail!\r\n");
    } else {
        LOG_I("register wifi event succeed!\r\n");
    }
}

static void OnWifiScanStateChangedHandler(int state, int size)
{
    if (size > 0) {
        ssid_count = size;
        g_staScanSuccess = 1;
    }
    LOG_I("callback function for wifi scan:%d, %d\r\n", state, size);
    return;
}

static void OnHotspotStaJoinHandler(StationInfo *info)
{
    (void)info;
    LOG_I("STA join AP\n");
    return;
}

static void OnHotspotStaLeaveHandler(StationInfo *info)
{
    (void)info;
    LOG_I("HotspotStaLeave:info is null.\n");
    return;
}

static void OnHotspotStateChangedHandler(int state)
{
    LOG_I("HotspotStateChanged:state is %d.\n", state);
    return;
}

static void WaitSacnResult(void)
{
    int scanTimeout = DEF_TIMEOUT;
    while (scanTimeout > 0) {
        sleep(ONE_SECOND);
        scanTimeout--;
        if (g_staScanSuccess == 1) {
            LOG_I("WaitSacnResult:wait success[%d]s\n", (DEF_TIMEOUT - scanTimeout));
            break;
        }
    }
    if (scanTimeout <= 0) {
        LOG_I("WaitSacnResult:timeout!\n");
    }
}

int BOARD_InitWifi(void)
{
    return 0;
}

int BOARD_ConnectWifi(const char *wifiSSID, const char *wifiPWD)
{
    return WifiConnect(wifiSSID, wifiPWD);
}

int BOARD_DisconnectWifi()
{
    Disconnect();
    return 0;
}

#define RSSI_LEVEL_MAX (-55)
#define RSSI_LEVEL_MIN (-90)

int BOARD_GetWifiSignalLevel(void)
{
    WifiLinkedInfo getLinkedRes;
    if(WIFI_SUCCESS != GetLinkedInfo(&getLinkedRes)) {
        LOG_E("%s %d: Get linked info faild!\r\n", __FUNCTION__, __LINE__);
        return -1;
    }
    if(RSSI_LEVEL_MAX < getLinkedRes.rssi) {
        return 4;
    }
    else if(RSSI_LEVEL_MIN > getLinkedRes.rssi) {
        return 1;
    }
    else {
        return (getLinkedRes.rssi / 15 + 7);
    }
}
