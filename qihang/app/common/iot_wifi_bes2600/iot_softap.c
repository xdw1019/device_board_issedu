/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <unistd.h>
#include "lwip/tcpip.h"
#include "lwip/ip_addr.h"
#include "lwip/netifapi.h"
#include "dhcps.h"
#include "wifi_hotspot.h"
#include "wifi_hotspot_config.h"
#include "iot_softap.h"
#include "iot_bes2600_debug.h"

#define    DEFAULT_AP_NAME    "TestIotAP"

static unsigned char   _mac_addr[6];
extern struct netif if_wifi_ap;

static void SetAddr(struct netif *pst_lwip_netif)
{
    ip4_addr_t st_gw;
    ip4_addr_t st_ipaddr;
    ip4_addr_t st_netmask;

    IP4_ADDR(&st_ipaddr, 192, 168, 51, 1);        // IP ADDR  为了和其他数字管家中的其他设备适配， 选择192.168.51网段
    IP4_ADDR(&st_gw, 192, 168, 51, 1);            // GET WAY ADDR
    IP4_ADDR(&st_netmask, 255, 255, 255, 0);    // NET MASK CODE

    netifapi_netif_set_addr(pst_lwip_netif, &st_ipaddr, &st_netmask, &st_gw);
}

static void ResetAddr(struct netif *pst_lwip_netif)
{
    ip4_addr_t st_gw;
    ip4_addr_t st_ipaddr;
    ip4_addr_t st_netmask;

    IP4_ADDR(&st_ipaddr, 0, 0, 0, 0);
    IP4_ADDR(&st_gw, 0, 0, 0, 0);
    IP4_ADDR(&st_netmask, 0, 0, 0, 0);

    netifapi_netif_set_addr(pst_lwip_netif, &st_ipaddr, &st_netmask, &st_gw);
}

int BOARD_SoftApStart(const char *ap_name)
{
    HotspotConfig config = {0};
    char *apName = ap_name;
    int retval;
    struct netif *p_netif = &if_wifi_ap;

    if (IsHotspotActive() == WIFI_HOTSPOT_ACTIVE) {
        LOG_E("WIFI_HOTSPOT_ACTIVE \n");
        return -1;
    }
    if (apName == NULL) {
        apName = DEFAULT_AP_NAME;
    }
    if (strcpy_s(config.ssid, sizeof(config.ssid), apName) != 0) {
        LOG_E("[sample] strcpy ssid fail.\n");
        return -1;
    }
    config.securityType = WIFI_SEC_TYPE_OPEN;
    retval = SetHotspotConfig((const HotspotConfig *)(&config));
    if (retval != WIFI_SUCCESS) {
        LOG_E("SetHotspotConfig \n");
        return -1;
    }
    LOG_I("%s %d\r\n", __FUNCTION__, __LINE__);
    retval = EnableHotspot();
    if (retval != WIFI_SUCCESS) {
        LOG_E("EnableHotspot failed! \n");
        return -1;
    }
    
    if (p_netif != NULL) {
        LOG_I("Start dhcp server action%s %d\r\n", __FUNCTION__, __LINE__);
        SetAddr(p_netif); // 设置网卡 IP信息
        dhcps_start(p_netif, NULL, 0);  // 启动dhcp server 
    }

    return 0;
}

void BOARD_SoftApStop(void)
{
    struct netif *p_netif = &if_wifi_ap;
    if (IsHotspotActive() == WIFI_HOTSPOT_NOT_ACTIVE) {
        LOG_E("WIFI_HOTSPOT_NOT_ACTIVE \n");
        return;
    }
    
    ResetAddr(p_netif);
    dhcps_stop(p_netif);
    DisableHotspot();
}
