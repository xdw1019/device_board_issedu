/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <securec.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#include "ohos_errno.h"
#include "ohos_types.h"

#include "schedule_store.h"

#define SCHEDULE_KEY    "/data/shcedule_info"
#define TEMP_KEY        "/data/info.tmp"

static int SearchScheduleInfo(const char *id)
{
    int idx = -1;
    int total = 0;
    int file;

    if (id == NULL) {
        return -1;
    }

    file = open(SCHEDULE_KEY, O_RDWR);
    if (file < 0) {
        return -1;
    }

    lseek(file, 0, SEEK_SET);
    if (read(file, (char *)&total, sizeof(int)) < 0) {
        close(file);
        return -1;
    }

    for (int i = 0; i < total; i++) {
        ScheduleInfo info;
        int off = sizeof(ScheduleInfo) * i + sizeof(int);

        lseek(file, off, SEEK_SET);

        if (read(file, (char *)&info, sizeof(ScheduleInfo)) < 0) {
            close(file);
            return -1;
        }

        if (strncmp((const char *)info.id, id, strlen(id)) == 0) {
            idx = i;
            break;
        }
    }

    close(file);

    return idx;
}

static int DeletScheduleStore(ScheduleInfo *info)
{
    int fd;
    int tmp;
    int idx;
    int total;
    if (info == NULL) {
        return -1;
    }

    idx = SearchScheduleInfo((const char *)info->id);
    if (idx < 0) {
        return -1;
    }

    fd = open(SCHEDULE_KEY, O_RDWR);
    if (fd < 0) {
        return -1;
    }

    lseek(fd, 0, SEEK_SET);
    if (read(fd, (char *)&total, sizeof(int)) < 0) {
        close(fd);
        return -1;
    }
    if (total <= 0) {
        close(fd);
        return -1;
    } else if (total == 1) {
        close(fd);
        unlink(SCHEDULE_KEY);
        return 0;
    }

    total -= 1;

    tmp = open(TEMP_KEY, O_CREAT | O_RDWR, 0);
    if (tmp < 0) {
        close(fd);
        return -1;
    }

    if (write(tmp, (char *)&total, sizeof(int)) < 0) {
        close(fd);
        close(tmp);
        return -1;
    }

    for (int i = 0; i <= total; i++) {
        ScheduleInfo tmpInfo;
        if (read(fd, (char *)&tmpInfo, sizeof(ScheduleInfo)) < 0) {
            close(fd);
            close(tmp);
            return -1;
        }
        if (i == idx) {
            continue;
        }
        if (write(tmp, (char *)&tmpInfo, sizeof(ScheduleInfo)) < 0) {
            close(fd);
            close(tmp);
            return -1;
        }
    }

    close(fd);
    close(tmp);

    unlink(SCHEDULE_KEY);
    rename(TEMP_KEY, SCHEDULE_KEY);

    return 0;
}

static int UpdateScheduleStore(ScheduleInfo *info)
{
    int fd, idx;

    if (info == NULL) {
        return -1;
    }

    idx = SearchScheduleInfo((const char *)info->id);
    if (idx < 0) {
        return -1;
    }

    fd = open(SCHEDULE_KEY, O_RDWR);
    if (fd < 0) {
        return -1;
    }

    if (lseek(fd, idx * sizeof(ScheduleInfo) + sizeof(int), SEEK_SET) < 0) {
        close(fd);
        return -1;
    }

    if (write(fd, (char *)info, sizeof(ScheduleInfo)) < 0) {
        close(fd);
        return -1;
    }

    close(fd);

    return 0;
}

static int AddScheduleStore(ScheduleInfo *info)
{
    int total = 0;
    int fd;

    if (info == NULL) {
        return -1;
    }

    fd = open(SCHEDULE_KEY, O_RDWR | O_CREAT | O_APPEND);
    if (fd < 0) {
        return -1;
    }
    lseek(fd, 0, SEEK_SET);
    if (read(fd, (char *)&total, sizeof(int)) < 0) {
        close(fd);
        return -1;
    }
    total += 1;
    lseek(fd, 0, SEEK_SET);
    if (write(fd, (char *)&total, sizeof(int)) < 0) {
        close(fd);
        return -1;
    }

    lseek(fd, 0, SEEK_END);
    if (write(fd, (char *)info, sizeof(ScheduleInfo)) < 0) {
        close(fd);
        return -1;
    }

    close(fd);

    return 0;
}

int ScheduleStoreUpdate(ScheduleInfo *info, SCHEDULE_OPTION option)
{
    int retval = -1;
    switch (option) {
        case SCHEDULE_OPTION_ADD:
            AddScheduleStore(info);
            break;
        case SCHEDULE_OPTION_UPDATE:
            UpdateScheduleStore(info);
            break;
        case SCHEDULE_OPTION_DELETE:
            DeletScheduleStore(info);
            break;
        default:
            break;
    }

}

int ScheduleStoreGetTotal(void)
{
    int total = 0;
    int fd = open(SCHEDULE_KEY, O_RDWR);
    if (fd < 0) {
        return -1;
    }

    if (read(fd, (char *)&total, sizeof(int)) < 0) {
        total = -1;
    }
    close(fd);

    return total;
}

int ScheduleStoreGetInfo(ScheduleInfo *info, int idx)
{
    int total = 0;
    int fd = open(SCHEDULE_KEY, O_RDWR);
    if (fd < 0) {
        return -1;
    }

    if (read(fd, (char *)&total, sizeof(int)) < 0) {
        close(fd);
        return -1;
    }

    if (idx >= total) {
        close(fd);
        return -1;
    }

    if (lseek(fd, idx * sizeof(ScheduleInfo) + sizeof(int), SEEK_SET) < 0) {
        close(fd);
        return -1;
    }

    if (read(fd, (char *)info, sizeof(ScheduleInfo)) < 0) {
        close(fd);
        return -1;
    }

    close(fd);

    return 0;
}

void ScheduleStoreDelete(void)
{
    unlink(SCHEDULE_KEY);
}
