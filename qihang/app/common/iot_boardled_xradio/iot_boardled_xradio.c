/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iot_demo_def.h"
#include "iot_boardled_xradio.h"
#include "ohos_init.h"
#include <cmsis_os2.h>

#define DEVICE_STATUS_LED_GPIO  21      // PA21

// the led part
int BOARD_InitIoLed(void)
{
    printf("LED INIT! \n");
    return 0;
}

int BOARD_SetIoLedStatus(int status)
{
    if (status) {
        GpioWrite(DEVICE_STATUS_LED_GPIO, 1);
    } else {
        GpioWrite(DEVICE_STATUS_LED_GPIO, 0);
    }
    return 0;
}

#define CONFIG_TASK_MAIN_STACKSIZE 0x800                 // main task stacksize must be bigger
#define CONFIG_TASK_MAIN_PRIOR 22                        // default task priority

/**
 @ brief Set the main task flash frequency
 */

typedef struct {
    int mode;
    osThreadId_t taskID;
}LedFlashController;
static LedFlashController g_ledFlashController;

int LedFlashFrequencySet(LED_FLASH_MODE mode)
{
    g_ledFlashController.mode = mode;

    return 0;
}

int LedFlashTaskDeinit(void)
{
    osThreadTerminate(g_ledFlashController.taskID);
    g_ledFlashController.taskID = NULL;
    return 0;
}

/**
 * @brief LED flashing task entry
 */
static void LedTaskEntry(const void *arg)
{
    const int ledFlashTime[LED_FLASH_MAX] = {
        100000, 300000, 500000, 1000000, 5000000
    };
    (void)arg;
    BOARD_InitIoLed();
    LedFlashFrequencySet(LED_FLASH_FAST_PLUGS);
    while (1) {
        if (g_ledFlashController.mode > LED_FLASH_SLOW_PLUGS) {
            g_ledFlashController.mode = LED_FLASH_SLOW_PLUGS;
        }
        BOARD_SetIoLedStatus(CN_BOARD_SWITCH_ON);
        if (g_ledFlashController.mode == LED_FLASH_SLOW_PLUGS) {
            usleep(ledFlashTime[LED_FLASH_SLOW]);
        } else {
            usleep(ledFlashTime[g_ledFlashController.mode]);
        }
        BOARD_SetIoLedStatus(CN_BOARD_SWITCH_OFF);
        usleep(ledFlashTime[g_ledFlashController.mode]);
    }
    return;
}

static void LedFlashTaskInit(void)
{
    osThreadAttr_t attr;
    RaiseLog(LOG_LEVEL_INFO, "DATA:%s Time:%s \r\n", __DATE__, __TIME__);
    // Create the IoT Main task
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = CONFIG_TASK_MAIN_STACKSIZE;
    attr.priority = CONFIG_TASK_MAIN_PRIOR;
    attr.name = "LedFlashTask";
    g_ledFlashController.taskID =  osThreadNew((osThreadFunc_t)LedTaskEntry, NULL, (const osThreadAttr_t *)&attr);
    if (g_ledFlashController.taskID == NULL) {
        RaiseLog(LOG_LEVEL_ERR, "Create the LED FLASH TASK failed");
    } else {
        RaiseLog(LOG_LEVEL_INFO, "Create the LED FLASH TASK success");
    }

    return;
}

void LedFlashTaskStart(void)
{
    LedFlashTaskInit();
}
