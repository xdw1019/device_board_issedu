# qihang案例开发

qihang 提供的例程多达 25个，每个例程都有非常详细的注释，代码风格统一，按照基本例程到高级例程的方式编排，方便初学者由浅入深逐步学习。开发者拿到工程后经过简单的编译和下载即可看到实验现象。

这些例程包括四个类别：内核类、基本外设类、驱动类、物联网类。不仅包括了硬件资源的应用，更是提供了丰富的物联网领域的应用示例，帮助物联网开发者更好更快地进行开发。

例程列表如下所示：

| 编号 | 类别   | 例程名           | 说明                                                         |
| ---- | ------ | ---------------- | ------------------------------------------------------------ |
| 01  | 入门 | hello world    |  [Hello World](01_KP_OS_HelloWorld/README.md)  |
| 02  | 内核   | task         |  [任务](02_KP_OS_Task/README.md)                                     |
| 03  | 内核   | mutex            | [互斥锁](03_KP_OS_Mutex/README.md) |
| 04  | 内核   | timer       | [定时器](04_KP_OS_Timer/README.md) |
| 05  | 内核   | event            | [事件](05_KP_OS_Event/README.md) |
| 06  | 内核   | semphore       | [信号量](06_KP_OS_Semp/README.md) |
| 07 | 内核 | led_blink        | [消息队列](07_KP_OS_Message/README.md) |
| 08 | 基础   | gpio           | [LED开关](08_KP_GPIO_led/README.md) |
| 09 | 基础   | pwm          | [ 呼吸灯](09_KP_PWM_led/README.md) |
| 10 | 基础   | button     | [按键控制](10_KP_KEY_led/README.md) |
| 11  | 基础   | adc       | [读取电压](11_KP_ADC_voltage/README.md) |
| 12 | E53传感器 | 烟雾   | [烟雾探测](12_KP_MQ2_example/README.md) |
| 13 | E53传感器   | 气体      | [可燃气体检测](13_KP_MQ7_example/README.md) |
| 14 | E53传感器   | 红外              | [人体感应](14_KP_INFRARED_example/README.md) |
| 15 | E53传感器   | NFC           | [碰一碰](15_KP_I2C_NFC/README.md) |
| 16 | E53传感器   | 心率      | [心率血氧](16_KP_HeartRate_example/README.md) |
| 17 | E53传感器   | 陀螺仪              | [智能健身](17_KP_SC7A20_example/README.md) |
| 18 | E53传感器 | 语音识别 | [智能语音](18_KP_UART/README.md) |
| 19 | E53传感器 | 指纹识别 | [智能门锁](19_KP_SmartDoor_example/README.md) |
| 20 | E53传感器 | GPS定位 | [GPS定位](20_KP_GPS_example/README.md) |
| 21  | E53传感器 | OLED显示  | [屏幕](21_KP_LCD_example/README.md) |
| 22 | E53传感器 | 电机驱动   | [智能风扇](22_KP_SHT30_example/README.md) |
| 23  | 物联网 | wifi ap    | [WIFI 热点](23_KP_WIFI_ap/README.md) |
| 24  | 物联网 | wifi sta   | [WIFI 接入](24_KP_WIFI_sta/README.md) |
| 25  | 物联网 | iot_mqtt             | [连接物联网云平台](25_KP_iot_cloud_oc/README.md) |
